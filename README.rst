~~~~~~~~~~~~~~~~~~~~~~
Polyominoes enumerator
~~~~~~~~~~~~~~~~~~~~~~

The files of this repository allows one to enumerate polyominoes based on
Jensen's algorithm. The code is written in Python, but it has been optimized
using Cython.

============
Dependencies
============

- `Python <http://python.org/>`__
- `Cython <http://cython.org/>`__
- `GCC <http://gcc.gnu.org/>`__

=======
Roadmap
=======

The first public release should at least have the following features added/bugs
corrected:

- Make sure the main script is functional and correct, in particular the
  installation.
- Add script ``configure`` for configuring the installation and/or compilation
  process. We can think of ``BASH_COMPLETION_DIR`` macro needed in the
  installation process.
- Get rid of the warning when compiling the project. These are the one I get::

    cc1plus: warning: command line option "-Wstrict-prototypes" is valid for
    C/ObjC but not for C++         
    enumeration.cpp: In function ‘__pyx_t_11enumeration_Site __pyx_convert__from_py___pyx_t_11enumeration_Si te(PyObject*)’:
    enumeration.cpp:18832: warning: ‘result.__pyx_t_11enumeration_Site::lower’ may be used uninitialized in this function
    enumeration.cpp:18832: warning: ‘result.__pyx_t_11enumeration_Site::upper’ may be used uninitialized in this function
    enumeration.cpp: In function ‘__pyx_t_11enumeration_Site __pyx_f_11enumeration_11PartialTree__transition (__pyx_obj_11enumeration_PartialTree*, int, int, int, int)’:
    enumeration.cpp:9502: warning: ‘__pyx_r’ may be used uninitialized in this function                     enumeration.cpp: In function ‘__pyx_t_11enumeration_Site __pyx_f_11enumeration_16PartialPolyomino__transition(__pyx_obj_11enumeration_PartialPolyomino*, int, int, int, int)’:
    enumeration.cpp:7482: warning: ‘__pyx_r’ may be used uninitialized in this function
    /usr/bin/g++-4.2 -bundle -undefined dynamic_lookup -L/opt/local/lib -Wl,-headerpad_max_install_names -L/ opt/local/lib/db46 build/temp.macosx-10.6-x86_64-2.7/enumeration.o -o /Users/alexandreblondinmasse/Docum ents/EnumerationPolyominos/enumeration.so

- Creates a test suite.
- Update the documentation (docstring, README, manpage).
- Simplify the choice of Python version (currently python3). Make sure the
  python version is set in order to avoid python conflicts such as using python2
  syntax and specifying ``#!/usr/bin/env python`` while ``python`` can mean
  python3 or python2 depending on the system you are.
- Clean ``int_repr`` (taking into account possible overflow).

=====
TODOs
=====

Other interesting features to keep in mind:

- Implements Knuth's algorithm for the minimum connectivity problem (see comment
  in source code). This will improve significantly the speed when enumerating
  polyominoes.
- Takes into account symmetry to improve speed of algorithm. It will also allow
  to generate free/fixed/one-sided polyominoes. Currently, only fixed
  polyominoes are generated.
- Add other types of installers (MacPorts, Homebrew, Windows, Debian package,
  ArchLinux). 
- Add a script for extracting statistics and classifying.
- Add a script for drawing.
- Improve speed for enumerating partially directed snakes (it uses a Python
  boolean list instead of a bool*). Moreover, the enumeration columnwise could
  discard more configurations than by looking only at the pillars.

==========
Known bugs
==========

None.

=========
Howto use
=========

In order for the script to run properly, the Cython library has to be compiled.
It's done by doing::

    make cython

From there, the script ``polyenum`` is used to call the enumeration code. You can
``./scripts/polyenum --help`` for help.

=======
Example
=======

-----------------------------------------
Enumerating all polyominoes of given area
-----------------------------------------

Let the area of the polyominoes be **4**. Finding all the polyominoes (not
inscribed in a particular rectangle) of area 4 is done as follows::

    $ ./polyenum -a 4 --fixed
    Fixed polyomino enumeration.
    XXXX

    X
    X
    X
    X

    XX
    XX

    X--
    XXX

    XX
    -X
    -X

    XXX
    X--

    XX
    X-
    X-

    -X-
    XXX

    -X
    XX
    -X

    -XX
    XX-

    -X
    XX
    X-

    --X
    XXX

    -X
    -X
    XX

    XX-
    -XX

    X-
    XX
    -X

    XXX
    -X-

    X-
    XX
    X-

    XXX
    --X

    X-
    X-
    XX

    Time elapsed: 0.000782012939453125 seconds
    Number of polyominoes: 19

More examples can be found reading polyenum's man page (``man ./doc/polyenum.1``).

==========
Installing
==========

The script can be installed on your system. This is useful since it provides
bash tab completion capability for the script. To install, simply do::

    make install
