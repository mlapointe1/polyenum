#coding=utf-8
#----------------------------------------------------------
# File: setup.py    Author(s): Alexandre Blondin Massé
#                              Simon Désaulniers
# Date: 2014-03-10
#----------------------------------------------------------
# Setup script for the polyomino enumeration python script.
#----------------------------------------------------------

from distutils.core import setup
from Cython.Distutils import build_ext
from Cython.Build import cythonize

setup(
        name = 'polyenum',
        version = '1.0',
        description = 'Set of enumerators of multiple types of polyominoes (snake, tree, etc.)',
        author = 'Alexandre Blondin Massé, Simon Désaulniers',
        author_email = 'alexandre.blondin.masse@gmail.com, rostydela@gmail.com',
        scripts = ['scripts/polyenum'],
        cmdclass = {'build_ext': build_ext},
        ext_modules = cythonize("polyenum/*.pyx")
)
