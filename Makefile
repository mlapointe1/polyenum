#--------------------------------------------------------
# File: Makefile    Author(s): Alexandre Blondin Massé
# Date: 2014-03-08 	       	   Simon Désaulniers
#--------------------------------------------------------

# module and script name
program=polyenum
package=$(program)
lib=enumeration

# installation commands
install=install -Dpv --group=root --owner=root
pysetup=python2.7 setup.py

# directories
man_dir=/usr/share/man/man1
bash_completion_dir=/usr/share/bash-completion/completions

.PHONY: clean cython
cython: $(package)/*.pyx $(package)/*.pxd
	$(pysetup) build_ext --inplace

clean:
	rm -rf build/ $(package)/*.c* $(package)/*.so

install:
ifneq ($(USER), root)
	@echo "You have to run this as root. Exiting..."
else
	@echo "Installng $(program)..."
	$(pysetup) install

	@echo "Configuring bash completion..."
	$(install) $(program)_bash_completion $(bash_completion_dir)/$(program) \
		|| echo "Cannot configure bash completion. See $$bash_completion_dir variable..." >&2

	@# installing documentation
	$(MAKE) install_doc
endif

install_doc:
	@echo "Installing documentation..."
	$(install) doc/$(program).1 $(man_dir)
	gzip $(man_dir)/$(program).1

uninstall:
	@echo "Uninstalling $(program)..."
	rm -rf /usr/lib/python2.7/site-packages/$(program)* \
		   /usr/bin/$(program) \
		   $(bash_completion_dir)/$(program)
	@echo "Removing documentation..."
	rm -f $(man_dir)/$(program).1.gz
reinstall: uninstall install
